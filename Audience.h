

#ifndef AUDIENCE_H
#define AUDIENCE_H

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include "opencv2/opencv.hpp"
#include "gui.h"
#include "defs.h"

using namespace cv;

class Audience{
    private:
        struct sockaddr_in serv_audaddr, serv_vidaddr;
        struct hostent *server;
        pthread_t vidthread;
        pthread_t audthread;
    
    public:
        char *hostname;
        unsigned int audioBufferSize;
        Gui *gui;
        int vidsockfd, audsockfd;
        int videoBytesHandled;
        int framesHandled;
        int audioBytesHandled;
        Audience(char* hostname, Gui *gui);
        virtual ~Audience();
        
        void readVideo();
        void readAudio();
};


#endif // AUDIENCE_H
