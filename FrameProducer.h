#ifndef FRAMEPRODUCER_H
#define FRAMEPRODUCER_H

#include <pthread.h>

#include "Frame.h"
#include "AudioInterface.h"
#include "Camera.h"
#include "Audience.h"
#include "defs.h"

class FrameProducer {

private:
    Camera* camera;
    void testRead();

public:
    Audience *audi;
    FrameProducer(Audience *audi);
    AudioInterface* audio;
    FrameProducer();
    char* readAudio();
    cv::Mat* readVideo();
    unsigned int audioBufferSize;
    Frame curFrame;

};

#endif // FRAMEPRODUCER_H
