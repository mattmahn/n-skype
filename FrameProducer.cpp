#include "FrameProducer.h"

FrameProducer::FrameProducer(Audience *audi){
    this->audi = audi;
}

FrameProducer::FrameProducer(){
    audi = NULL;
    camera = new Camera(VIDEO_WIDTH, VIDEO_HEIGHT);
    //TODO Change this to 1 when on the BB
    #if onVm == 0
        audio = new AudioInterface("plughw:1", SAMPLING_RATE, NUMBER_OF_ACHANNELS,SND_PCM_STREAM_CAPTURE);
    #else
        audio = new AudioInterface("plughw:0", SAMPLING_RATE, NUMBER_OF_ACHANNELS,SND_PCM_STREAM_CAPTURE);
    #endif
    
    audio->open();
    this->audioBufferSize = audio->getRequiredBufferSize();
    curFrame.audio = new char[audioBufferSize];
    curFrame.video = new cv::Mat();
}

char* FrameProducer::readAudio() {
    audio->read(curFrame.audio);
    return curFrame.audio;
}

cv::Mat* FrameProducer::readVideo() {
    camera->takePicture(curFrame);
    return curFrame.video;
}

void FrameProducer::testRead(){
    unsigned int i;

    cout<<"Audio for current frame is ";
    //Print out the audio buffer each read
    for(i = 0; i<10; i++){
        cout<<curFrame.audio[i]<<" ";
    }
    cout<<'\n';
    cout<<"Video for current frame is ";
    cout<<curFrame.video;
    cout<<'\n';
}
