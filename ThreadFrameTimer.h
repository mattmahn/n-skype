
#ifndef _THREAD_FRAME_TIMER_
#define _THREAD_FRAME_TIMER_

class ThreadFrameTimer {
    private:
        double totalExecutionTime;
        int executionCounts;
    public:
        ThreadFrameTimer();
        void update_execution_time(double executionTime);
        double average_execution_time();
};

#endif
