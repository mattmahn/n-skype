/*
 * audience.cpp
 *
 *  Created on: Apr 7, 2016
 *      Author: minorr
 */
 
 /*
  * Build this with:
  * g++ -lpthread -I `pkg-config --libs --cflags opencv` -o Audience Audience.cpp
  */
#include "Audience.h"
#include <sys/time.h>

  
using namespace std;
using namespace cv;

void error(char const *msg){
    perror(msg);
    exit(-1);
}

static void* staticReadVideo(void* p) {
    static_cast<Audience*>(p)->readVideo();
    return NULL;
}

static void* staticReadAudio(void* p) {
    static_cast<Audience*>(p)->readAudio();
    return NULL;
}

Audience::Audience (char* hostname, Gui *gui){
    this->framesHandled = 0;
    this->videoBytesHandled = 0;
    this->audioBytesHandled = 0;
    int recThread;
    this->hostname = hostname;
    this->gui = gui;
    
    this->vidsockfd = socket(AF_INET, SOCK_STREAM, 0);
    this->audsockfd = socket(AF_INET, SOCK_STREAM, 0);
    this->server = gethostbyname(this->hostname);
    
    memset((void*)&this->serv_audaddr, 0, sizeof(this->serv_audaddr));
    memset((void*)&this->serv_vidaddr, 0, sizeof(this->serv_vidaddr));
    
    this->serv_audaddr.sin_family = AF_INET;
    this->serv_vidaddr.sin_family = AF_INET;
    
    memcpy((void*)&this->serv_audaddr.sin_addr.s_addr, (void*)this->server->h_addr, this->server->h_length);
    memcpy((void*)&this->serv_vidaddr.sin_addr.s_addr, (void*)this->server->h_addr, this->server->h_length);
    
    this->serv_vidaddr.sin_port = htons(VIDEO_PORT);
    this->serv_audaddr.sin_port = htons(AUDIO_PORT);
    
    recThread = pthread_create(&this->vidthread, NULL, staticReadVideo, this);
    recThread = pthread_create(&this->audthread, NULL, staticReadAudio, this);
}

Audience::~Audience(){}

void Audience::readVideo() {
    connect(this->vidsockfd, (struct sockaddr *)&this->serv_vidaddr, sizeof(this->serv_vidaddr));
    
    unsigned int rows, cols;
    int n;
    
    n = read(this->vidsockfd, &rows, sizeof(rows));
    if (n < 0) {
        error("AUDIENCE ERROR while reading row count");
    }
    n = read(this->vidsockfd, &cols, sizeof(cols));
    if (n < 0) {
        error("AUDIENCE ERROR while reading column count");
    }

    Mat img = Mat::zeros(rows, cols, CV_8UC3);
    int imgSize = img.total()*img.elemSize();
    
    uchar sockData[imgSize];
    
    while (1==1) {
        //Clear the video buffer
        memset((void*) &sockData[0], 0, imgSize);
        
        int videoBytes = 0;
        for (int i = 0; i < imgSize; i += videoBytes) {
            if ((videoBytes = recv(this->vidsockfd, sockData + i, imgSize - i, 0)) == -1) {
                cout << "Failed to take video data" << endl;
                exit(2);
            }
        }
        this->videoBytesHandled+=videoBytes;
        this->framesHandled++;
        int ptr = 0;
        Mat tImg;
        tImg = Mat::zeros(rows, cols, CV_8UC3);
        for (int i = 0; i < tImg.rows; i++) {
            for (int j = 0; j < tImg.cols; j++) {
                tImg.at<cv::Vec3b>(i,j) = cv::Vec3b(sockData[ptr+0], sockData[ptr+1], sockData[ptr+2]);
                ptr += 3;
            }
        }
        gui->playCounts(this->framesHandled,this->audioBytesHandled+this->videoBytesHandled);
        gui->playVideo(&tImg);
    }
}

void Audience::readAudio() {
    connect(this->audsockfd, (struct sockaddr *)&this->serv_audaddr, sizeof(this->serv_audaddr));
    
    char* audioBuffer;
    int n;
    
    n = read(this->audsockfd, &this->audioBufferSize, sizeof(this->audioBufferSize));
    if (n < 0) {
        error("AUDIENCE ERROR when trying to read handshake");
    }
    
    cout << "Server reports buffer size as " << this->audioBufferSize << endl;
    
    n = write(this->audsockfd, &this->audioBufferSize, sizeof(this->audioBufferSize));
    if (n < 0) {
        error("AUDIENCE ERROR when replying to server on audio buffer size");
    }
    
    audioBuffer = new char[audioBufferSize];
    
    while (1==1) {
        memset((void*) &audioBuffer[0], 0, audioBufferSize);
        
        int audioBytes = 0;
        if ((audioBytes = recv(this->audsockfd, audioBuffer, audioBufferSize, 0)) == -1) {
            cout << "Failed to take audio data" << endl;
            exit(2);
        }
        this->audioBytesHandled+=audioBytes;
        gui->playSound(audioBuffer, audioBytes);
    }
}

/*
void Audience::readThread() {
    
    while (1==1) {
        //int videoBytes = 0;
    //    for (int i = 0; i < imgSize; i += videoBytes) {
    //        if ((videoBytes = recv(sockfd, sockData +i, imgSize  - i, 0)) == -1) {
    //            cout << "Failed to take data" << endl;
        //        exit(2);
    //        }
    //    }
        
        //int ptr = 0;
        //Mat tImg;
        //tImg = Mat::zeros(rows, cols, CV_8UC3);
        //for (int i = 0; i < tImg.rows; i++) {
        //    for (int j = 0; j < tImg.cols; j++) {
        //        tImg.at<cv::Vec3b>(i,j) = cv::Vec3b(sockData[ptr+0],
        //            sockData[ptr+1], sockData[ptr+2]);
        //        ptr += 3;
        //    }
        //}
        
    }
}*/

