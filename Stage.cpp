#include "Stage.h"

static void* staticGuiThread(void* p) {
    static_cast<Stage*>(p)->updateGuiThread();
    return NULL;
}


/**
 * Stage class for controlling and initializing nearly everything for the server.
 *
 * @author Joshua Davies
 */
Stage::Stage(){
    producer = new FrameProducer();
    sen = new FrameSender(producer);
    sen->start();
    player = new Gui();
    player->setAudioInterface(producer->audio);
    player->show();
    pthread_t guiThread;
    pthread_create(&guiThread, NULL, staticGuiThread, this);
}

/**
 * Function called by pthread to update the GUI and draw to the screen.
 */
void Stage::updateGuiThread(){
    cv::Mat copyVideoMat;
    while(true){
        this->producer->curFrame.video->copyTo(copyVideoMat);
        this->player->playCounts(this->sen->framesHandled,this->sen->videoBytesHandled+this->sen->audioBytesHandled);
        this->player->playVideo(&copyVideoMat);
        usleep(GUI_SLEEP);
    }
}

/**
 * Returns a reference to the Gui object used by the Stage.
 */
Gui* Stage::getGui(){
    return player;
}
