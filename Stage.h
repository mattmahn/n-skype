#ifndef STAGE_H
#define STAGE_H
#include "FramePlayer.h"
#include "FrameProducer.h"
#include "FrameSender.h"
#include "gui.h"
#include "defs.h"

/**
 * Stage class that controls pretty much everything for the server.
 *
 * @author Joshua Davies
 */
class Stage{
private:
    Gui *player;
    FrameSender* sen;
    
public:
    FrameProducer *producer;
    Stage();
    Gui* getGui();
    void updateGuiThread();
};

#endif // STAGE_H

