#include "gui.h"

using namespace std;

Gui::Gui()
{
    // setup audio interface
    imageLabel = new QLabel();
    framesLabel = new QLabel();
    bytesLabel = new QLabel();
    QVBoxLayout *layout = new QVBoxLayout;
    QWidget *window = new QWidget();
    layout->addWidget(imageLabel);
    layout->addWidget(framesLabel);
    layout->addWidget(bytesLabel);
    window->setLayout(layout);
    setCentralWidget(window);
    connect(this,SIGNAL(updateImage()),this,SLOT(setImage()));
    connect(this,SIGNAL(updateCounts(int,int)),this,SLOT(setCounts(int,int)));

}

Gui::~Gui()
{
    audioIface->close();
    delete audioIface;
}

void Gui::setImage(){
    QPixmap px = QPixmap::fromImage(mRenderQtImg);
    imageLabel->setPixmap(px);
}

void Gui::createAudioInterface(){
    #if onVm == 0
        audioIface = new AudioInterface("plughw:1",SAMPLING_RATE, NUMBER_OF_ACHANNELS,SND_PCM_STREAM_PLAYBACK);
    #else
        audioIface = new AudioInterface("plughw:0",SAMPLING_RATE, NUMBER_OF_ACHANNELS,SND_PCM_STREAM_PLAYBACK);
    #endif
    audioIface->open();
}

void Gui::setAudioInterface(AudioInterface *audi){
    audioIface = audi;
}

void Gui::playSound(char *audio, unsigned int audioBuffSize){
    audioIface->write(audio,audioBuffSize);
}

void Gui::setCounts(int frames, int bytes){
    QString framesText("Frames: ");
    framesText.append(QString::number(frames));
    framesLabel->setText(framesText);
    QString bytesText("Bytes: ");
    bytesText.append(QString::number(bytes));
    bytesLabel->setText(bytesText);
}

void Gui::playCounts(int frames, int bytes){
    emit updateCounts(frames,bytes);
}

void Gui::playVideo(cv::Mat *img){
    double mImgRatio = (double)img->cols / (double)img->rows;
    if (img->channels() == 3) {
    mRenderQtImg = QImage((const unsigned char*) img->data,
                         img->cols, img->rows,
                         img->step, QImage::Format_RGB888).rgbSwapped();
    } else if (img->channels() == 1) {
    mRenderQtImg = QImage((const unsigned char*) img->data,
                         img->cols, img->rows,
                         img->step, QImage::Format_Indexed8);
    } else {
        //std::cerr << "I don't know what to do with an image of this many channels" << std::endl;
    }
    emit updateImage();
}
