#ifndef FRAMESENDER_H
#define FRAMESENDER_H

#include <netdb.h>
#include <stdio.h>
#include <pthread.h>
#include "FrameProducer.h"
#include "defs.h"

/**
 * Class definition for the FrameSender class.
 */
class FrameSender {

private:
    //sockrc: return from pthread_create
    int sockrc, cliaudlen, clividlen;
    pthread_t audiothread;
    pthread_t videothread;
    struct sockaddr_in aud_addr, vid_addr, cliaud_addr, clivid_addr;
    struct hostent *server;

public:
    FrameSender(FrameProducer* producer);
    virtual ~FrameSender();
    virtual void start();
    bool runthread;
    int serv_vidsockfd, serv_audsockfd, audsockfd, vidsockfd;
    FrameProducer* producer;
    
    void writeAudio();
    void writeVideo();
    int videoBytesHandled;
    int framesHandled;
    int audioBytesHandled;
};
static void* writeThread(void *arg);

#endif // FRAMESENDER_H
