#include "main.h"

#include "defs.h"
#include "mainwindow.h"
#include "Stage.h"
#include "Audience.h"

Gui *g;

int main(int argc, char *argv[])
{
    // Set process priority to as high as possible
    // WARNING - this program needs to be run as root for this command to execute
    setpriority(PRIO_PROCESS, 0, -20);
    QApplication a(argc, argv);
    
    if(argc>1){
        g = new Gui();
        Audience *audience = new Audience(argv[1],g);
        g->createAudioInterface();
        g->show();
        int res = a.exec();
    }
    else{
        Stage *stage = new Stage();
        g = stage->getGui();
        int res = a.exec();
    }

}
