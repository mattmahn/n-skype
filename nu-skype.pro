#-------------------------------------------------
#
# Project created by QtCreator 2016-05-11T23:10:10
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += link_pkgconfig
PKGCONFIG += opencv
LIBS += -lasound

TARGET = nu-skype
TEMPLATE = app

INCLUDEPATH += /usr/local/include/opencv

SOURCES += main.cpp\
    AudioInterface.cpp \
    Camera.cpp \
    FrameProducer.cpp \
    GPIO.cpp \
    gui.cpp \
    Stage.cpp \
    FrameSender.cpp \
    ThreadFrameTimer.cpp \
    Audience.cpp

HEADERS  += \
    Audience.h \
    AudioInterface.h \
    Camera.h \
    foo.h \
    Frame.h \
    FramePlayer.h \
    FrameProducer.h \
    FrameReceiver.h \
    FrameSender.h \
    GPIO.h \
    Stage.h \
    gui.h \
    main.h \
    defs.h

FORMS    +=
