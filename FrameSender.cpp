#include "FrameSender.h"

void fs_error(char const *msg) {
    perror(msg);
    exit(-1);
}

FrameSender::FrameSender(FrameProducer* producer) {
    this->framesHandled = 0;
    this->videoBytesHandled = 0;
    this->audioBytesHandled = 0;
    this->producer = producer;
    this->runthread = false;

    // Open sockets for both the audio and video streams
    this->serv_vidsockfd = socket(AF_INET, SOCK_STREAM, 0);
    this->serv_audsockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (this->serv_vidsockfd < 0) {
        fs_error("FS_ERROR opening video server socket");
    }
    if (this->serv_audsockfd < 0) {
        fs_error("FS_ERROR opening audio server socket");
    }

    // Get the hostname
    this->server = gethostbyname(SERVER_LISTENING_HOST);

    // If the server is NULL, there is no such host.
    if (server == NULL) {
        fs_error("FS_ERROR, host not correct\n");
    }

    // Initialize the buffer to all zeroes
    memset((void*)&(this->aud_addr), 0, sizeof(this->aud_addr));
    memset((void*)&(this->vid_addr), 0, sizeof(this->vid_addr));

    // Set both sockets to use TCP/IP
    this->aud_addr.sin_family = AF_INET;
    this->vid_addr.sin_family = AF_INET;
    
    this->aud_addr.sin_addr.s_addr = INADDR_ANY;
    this->vid_addr.sin_addr.s_addr = INADDR_ANY;
    
    // Set socket ports to pre-defined defaults (3010 for audio, 3011 video)
    this->aud_addr.sin_port = htons(AUDIO_PORT);
    this->vid_addr.sin_port = htons(VIDEO_PORT);

    if (bind(this->serv_audsockfd, (struct sockaddr *) &(this->aud_addr),
            sizeof(this->aud_addr)) < 0) {
        fs_error("FS_ERROR on audio binding");
    }
    if (bind(this->serv_vidsockfd, (struct sockaddr *) &(this->vid_addr),
            sizeof(this->vid_addr)) < 0) {
        fs_error("FS_ERROR on video binding");
    }

    // Listen on the socket for an incoming connection. The parameter is the
    // number of connections that can be waiting or queued up.
    listen(this->serv_audsockfd, 1);
    listen(this->serv_vidsockfd, 1);
    this->cliaudlen = sizeof(this->cliaud_addr);
    this->clividlen = sizeof(this->clivid_addr);
}

FrameSender::~FrameSender() {
    close(this->serv_audsockfd);
    close(this->serv_vidsockfd);
    close(this->audsockfd);
    close(this->vidsockfd);
}

static void* staticStartupAudio(void* p) {
    static_cast<FrameSender*>(p)->writeAudio();
    return NULL;
}

static void* staticStartupVideo(void* p) {
    static_cast<FrameSender*>(p)->writeVideo();
    return NULL;
}

void FrameSender::start() {
    this->runthread = true;
    this->sockrc = pthread_create(&(this->audiothread), NULL, staticStartupAudio, this);
    if (this->sockrc) {
        fs_error("ERROR on audio thread creation.");
    }
    
    this->sockrc = pthread_create(&(this->videothread), NULL, staticStartupVideo, this);
    if (this->sockrc) {
        fs_error("ERROR on video thread creation.");
    }
}

void FrameSender::writeAudio() {
    
    // Accept any audio connections
    this->audsockfd = accept(this->serv_audsockfd,
                             (struct sockaddr *) &(this->cliaud_addr),
                             (socklen_t*) &(this->cliaudlen));
    if (this->audsockfd < 0) {
        fs_error("ERROR on client accept");
    }
    
    int n;
    int handshake_buffer[10];
    char* currAudio;
    
    // Write the buffer size over to the client
    unsigned int actualBufferSize = this->producer->audioBufferSize;
    n = write(this->audsockfd, &actualBufferSize, sizeof(actualBufferSize));
    if (n < 0) {
        fs_error("ERROR writing audio buffer to socket");
    }
    
    memset(&handshake_buffer[0], 0, sizeof(handshake_buffer));
    
    // Read *anything* back from the client, to confirm that streaming is ready
    n = read(this->audsockfd, handshake_buffer, sizeof(int));
    if (n < 0) {
        fs_error("ERROR reading handshake back from client");
    }
    
    while(this->runthread) {
        // Get audio from the producer
        currAudio = this->producer->readAudio();
        // Write the gathered audio to the client
        n = write(this->audsockfd, currAudio, actualBufferSize);
        this->audioBytesHandled+=n;
        if (n < 0) {
            fs_error("ERROR writing audio to socket");
        }
        
    }
}

void FrameSender::writeVideo() {
    cv::Mat* currVideo;
    
    // Accept any video connections
    this->vidsockfd = accept(this->serv_vidsockfd,
                             (struct sockaddr *) &(this->clivid_addr),
                             (socklen_t*) &(this->clividlen));
    if (this->vidsockfd < 0) {
        fs_error("ERROR on client accept");
    }
    
    int n;
    
    // Read a test video frame, for info gathering purposes
    currVideo = producer->readVideo();
    
    // Write the known image height
    n = write(this->vidsockfd, &currVideo->rows, sizeof(currVideo->rows));
    if (n < 0) {
        fs_error("ERROR writing video row length to socket");
    }
    
    // Write the known image width
    n = write(this->vidsockfd, &currVideo->cols, sizeof(currVideo->cols));
    if (n < 0) {
        fs_error("ERROR writing video col length to socket");
    }
    
    while(this->runthread) {
        // Read the video from the producer
        currVideo = producer->readVideo();
        
        // Write *all* of the video to the client
        n = send(this->vidsockfd, currVideo->data, currVideo->total() * currVideo->elemSize(), 0);
        this->videoBytesHandled+=n;
        this->framesHandled+=1;
    }
}
