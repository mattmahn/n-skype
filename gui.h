#ifndef GUI_H
#define GUI_H

#include <opencv2/core/core.hpp>
#include <QLabel>
#include <QGLWidget>
#include <QMainWindow>
#include <QVBoxLayout>

#include "AudioInterface.h"
#include "defs.h"

using namespace std;

/**
 * Class definition for the Gui class.
 *
 */
class Gui : public QMainWindow
{
    Q_OBJECT

public:
    explicit Gui();
    ~Gui();
    void playVideo(cv::Mat*);
    void playSound(char*, unsigned int);
    void createAudioInterface();
    void setAudioInterface(AudioInterface *audi);
    void playCounts(int frames, int bytes);

private:
    AudioInterface *audioIface;
    QLabel *imageLabel;
    QLabel *framesLabel;
    QLabel *bytesLabel;
    /// Qt image to be rendered
    QImage mRenderQtImg;
    /// original OpenCV image to be shown
    cv::Mat mOrigImage;
 public slots:
    void setImage();
    void setCounts(int frames,int bytes);
 signals:
    void updateImage();
    void updateCounts(int frames,int bytes);

};

#endif // GUI_H
