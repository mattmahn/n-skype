#ifndef _NU_SKYPE_DEFS_
#define _NU_SKYPE_DEFS_

/*
Definitions file used by rest of program.
*/

// 0 if not on VM, anything else if on VM
#define onVm (0)

// Default listening hostname for server
#define SERVER_LISTENING_HOST ("0.0.0.0")

#define AUDIO_PORT (3010)
#define VIDEO_PORT (3011)

#define SAMPLING_RATE (8000)
#define NUMBER_OF_ACHANNELS (1)

#define VIDEO_WIDTH (160)
#define VIDEO_HEIGHT (120)

#define GUI_SLEEP (50000)
#endif //_NU_SKYPE_DEFS_
