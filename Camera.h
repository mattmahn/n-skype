/*
 * Camera.h
 *
 *  Created on: Apr 13, 2016
 *      Author: se3910
 */

#ifndef CAMERA_H_
#define CAMERA_H_
#include "opencv2/opencv.hpp"
#include "GPIO.h"
#include  "Frame.h"
#include <stdio.h>
#include <stdlib.h>

#include "defs.h"

using namespace exploringBB;
using namespace cv;
using namespace std;

class Camera {
public:
    Camera(int width, int height);
    virtual ~Camera();
    void takePicture(Frame &frame);
    void shutdown();
private:
    int width;
    int height;
    VideoCapture* capture;
    cv::Mat mat;
};

#endif /* CAMERA_H_ */
