#include "ThreadFrameTimer.h"

ThreadFrameTimer::ThreadFrameTimer() {
    this->totalExecutionTime = 0.0;
    this->executionCounts = 0;
}

void ThreadFrameTimer::update_execution_time(double executionTime) {
    if (executionTime < 0) {
        return;
    }
    this->totalExecutionTime += executionTime;
    this->executionCounts += 1;
}

double ThreadFrameTimer::average_execution_time() {
    if (this->executionCounts == 0) {
        return 0.0;
    } else {
        return (this->totalExecutionTime / this->executionCounts);
    }
}
