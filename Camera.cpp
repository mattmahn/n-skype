/*
 * Camera.cpp
 *
 *  Created on: Apr 13, 2016
 *      Author: se3910
 */

#include "Camera.h"

Camera::Camera(int width, int height){
	this->width = width;
	this->height = height;
	capture = new VideoCapture(0);
	capture->set(CV_CAP_PROP_FRAME_WIDTH,width);
	capture->set(CV_CAP_PROP_FRAME_HEIGHT,height);
    mat = cv::Mat(VIDEO_HEIGHT,VIDEO_WIDTH,3);
}

Camera::~Camera() {
	// TODO Auto-generated destructor stub
}

void Camera::takePicture(Frame &frame){
    #if onVm == 0
        (*capture) >> mat;
    #else
        mat = imread("images.jpeg");
    #endif
    frame.video = &mat;
}
void Camera::shutdown(){
	capture->release();
}
